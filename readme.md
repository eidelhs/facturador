# **FACTURADOR PRO V 2.0**

## Acerca de FACTURALATAM

Estamos enfocados en crear herramientas y soluciones para desarrolladores de software, empresas medianas y pequeñas, tenemos un sueño, queremos democratizar la facturación electrónica.

## Instalación del Facturador Pro

Para conocer el proceso de instalación del facturador, visite la documentación

[Manual - Forge](https://docs.google.com/document/d/1NVV2yv8ys8tjdz5ByvEPhIUdm0GJ95XiB-iMlElaJTA/edit# "Clic")
<br>
[Manual - Windows - Linux](https://drive.google.com/open?id=1Jf0vgGvx27MbOB4JYsk9Gzgd5QIl32j35pwU1LI_Woo "Clic")
<br>
[Manual - Docker - Linux](https://docs.google.com/document/d/1E8jOrnbASjzBqhvgjhlJdhHExUjq4A0DoRKhkFEuFyY/edit?usp=sharing "Clic")

## Manejo del facturador

Visite el [Manual de usuario](https://drive.google.com/open?id=13t2-eqf57QRbAFR8qPkAjOXjODx6BCfWVjmzmrcHE50 "Clic")

## Manejo de la API

Para conocer acerca de nuestra API, visite la [documentación](https://docs.google.com/document/d/1FtuEGAq7scoQFGQKBuT8Zd6dLVU7h9abeGXbE41J-qA/edit# "Clic")<br>

Además puede realizar sus pruebas usando [Postman Collection](https://drive.google.com/open?id=1wilZDBDaH2_8eRFFcH56w_U1Naba2TZ6 "Clic") 

## Pruebas

[Manual de pruebas](https://docs.google.com/document/d/1GihGu-qNukj27hufdkPWUYO6wl-XIUVyK2cXygDqdlI/edit# "Clic")

## ADMIN

Visite el siguiente enlace: [FACTURADOR PRO](http://2facturaloperuonline.com "Clic")
<br>
Usuario: admin@gmail.com<br>
Contraseña: 123456

## DEMO

Visite el siguiente enlace: [FACTURADOR PRO](http://demo.2facturaloperuonline.com "Clic")
<br>
Usuario: demo@gmail.com<br>
Contraseña: 123456

## Actualización Docker-Git

Visite el siguiente enlace: [Guía](https://docs.google.com/document/d/11PI1a9yjCPfH9CCuWmJSrdj1V8IEUffqurqvdkw29co/edit?usp=sharing "Clic")

## Cambio de entornos (Prueba - Producción)

Visite el siguiente enlace: [Guía](https://docs.google.com/document/d/1iHUrlqzjwoQm967PRYirX5SN4L2ONAH8yvwTcePZ1PU/edit?usp=sharing "Clic")
 
## Patrocinadores FACTURADOR PRO V 2.0

 - **[FACTURALATAM](http://facturalatam.com/)** 
 - **[CursosDev](http://cursosdev.com/)** 

## Contribución
 
Gracias por considerar contribuir al facturador pro

## Vulnerabilidades de seguridad

Si descubre una vulnerabilidad de seguridad en la API, envíe un correo electrónico a facturalatamglobal@gmail.com . Las vulnerabilidades de seguridad serán tratadas con prontitud.


